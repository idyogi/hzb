Langkah Menggunakan Token

Login Aplikasi di

/api/login [param:email, password]

email : robbyn@test.com, pedy@test.com, yogi@test.com
password : secret

return ['status' => 'success', token => 'jsdlajdlkasd']

yang sudah menggunakan Token

/api/get-fav
/api/add-fav/{id}
/api/remove-fav/{id}

Cara Memastikan User mana yang login
/api/profile

Token dikasih ke Header
X-Authorization:$2y$10$EVbTEtjenhQF99wAxV/c0ubUzSqYIWElGq3SRVQqn9v0s9wy.aNmG (tokennya)