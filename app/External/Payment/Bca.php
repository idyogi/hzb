<?php

namespace App\External\Payment;

class Bca
{
    // @method \Bca\BcaThhp
    public $bca;
    private $access_token = false;

    public function __construct()
    {
        $options = array(
            'scheme'        => 'https',
            'port'          => 443,
            'host'          => 'sandbox.bca.co.id',
            'timezone'      => 'Asia/Jakarta',
            'timeout'       => 30,
            'debug'         => true,
            'development'   => true
        );

        $corp_id = "BCAAPI2016";
        $client_key = "c9559c49-63b4-450a-bed2-4a625a7cd56e"; // benar
        $client_secret = "3338e795-0974-4b3b-ae64-41e4ee80a8bd"; // benar
        $apikey = "5e25ccd7-83a8-424f-bfcf-9b18a7c48a91";
        $secret = "b4e5aa4b-2da4-4f2c-a171-a609abc72977";
        \Bca\BcaHttp::setTimeZone('Asia/Jakarta');
        $this->bca = new \Bca\BcaHttp($corp_id, $client_key, $client_secret, $apikey, $secret, $options);
    }

    public function login()
    {
        return $this->bca->httpAuth();
    }

    public function getToken()
    {
        if ($this->access_token) return $this->access_token;

        $response = $this->login();
        if (!isset($response->body->access_token)) {
            throw new Exception("Invalid Login", 1);
        }
        return $this->access_token = $response->body->access_token;
    }

    public function getBalance($account)
    {
        return $this->bca->getBalanceInfo($this->getToken(), $account);
    }
}
