<?php

namespace App\External\Ongkir;

use Curl\Curl;

/**
 *  Ongkir RajaOngkir
 */
class RajaOngkir
{
    private $api_key;
    private $base_url;

    public function __construct()
    {
        $this->api_key = config('ongkir.key');
        $this->base_url = config('ongkir.base_url');
    }

    /**
     * Method "province" digunakan untuk mendapatkan daftar propinsi yang ada di Indonesia.
     * @param  int $id optional, id privinsi
     * @return \Illuminate\Support\Collection
     */
    public function getProvince($id = null)
    {
        $data = $id ? ['id' => $id] : [];

        $curl = new Curl();
        $curl->setHeader('key', $this->api_key);
        $curl->get($this->base_url . '/province', $data);
        if (!$curl->error) {
            return collect($curl->response->rajaongkir->results);
        }
        return collect([]);
    }

    /**
     * Method "city" digunakan untuk mendapatkan daftar kota/kabupaten yang ada di Indonesia.
     * @param  int $province_id
     * @param int $id
     * @return \Illuminate\Support\Collection
     */
    public function getCity($province_id = null, $id = null)
    {
        $data = [];
        if ($province_id) {
            $data = array_merge($data, ['province' => $province_id]);
        }
        if ($id) {
            $data = array_merge($data, ['id' => $id]);
        }

        $curl = new Curl();
        $curl->setHeader('key', $this->api_key);
        $curl->get($this->base_url . '/city', $data);
        if (!$curl->error) {
            return collect($curl->response->rajaongkir->results);
        }
        return collect([]);
    }

    /**
     * Method "subdistrict" digunakan untuk mendapatkan daftar kecamatan yang ada di Indonesia.
     * @param  int $city_id
     * @param  int $id
     * @return \Illuminate\Support\Collection
     */
    public function getSubdistrict($city_id, $id = null)
    {
        $data = [];
        if ($city_id) {
            $data = array_merge($data, ['city' => $city_id]);
        }
        if ($id) {
            $data = array_merge($data, ['id' => $id]);
        }

        $curl = new Curl();
        $curl->setHeader('key', $this->api_key);
        $curl->get($this->base_url . '/subdistrict', $data);
        if (!$curl->error) {
            return collect($curl->response->rajaongkir->results);
        }
        return collect([]);
    }

    /**
     * Method “cost” digunakan untuk mengetahui tarif pengiriman (ongkos kirim) dari dan ke kecamatan tujuan tertentu dengan berat tertentu pula.
     * @param  int $origin  ID kota/kabupaten atau kecamatan asal
     * @param  string $originType   Tipe origin: 'city' atau 'subdistrict'.
     * @param  int $destination ID kota/kabupaten atau kecamatan tujuan
     * @param  string $destinationType Tipe destination: 'city' atau 'subdistrict'.
     * @param  int $weight  Berat kiriman dalam gram
     * @param  string $courier  Kode kurir: jne, pos, tiki, rpx, esl, pcp, pandu, wahana, sicepat, jnt, pahala, cahaya, sap, jet, indah, dse, slis, first, ncs, star, ninja, lion, idl.
     * @return \Illuminate\Support\Collection
     */
    public function getCost($origin, $originType, $destination, $destinationType, $weight, $courier = 'jne:tiki:jnt:sicepat')
    {
        $data = [
            'origin' => $origin,
            'originType' => $originType,
            'destination' => $destination,
            'destinationType' => $destinationType,
            'weight' => $weight,
            'courier' => $courier,
        ];
        $curl = new Curl();
        $curl->setHeader('key', $this->api_key);
        $curl->post($this->base_url . '/cost', $data);
        if (!$curl->error) {
            return collect($curl->response->rajaongkir->results);
        }
        return collect([]);
    }

    /**
     * Method “waybill” untuk digunakan melacak/mengetahui status pengiriman berdasarkan nomor resi.
     * @param  [type] $resi    [description]
     * @param  [type] $courier [description]
     * @return \Illuminate\Support\Collection
     */
    public function track($resi, $courier)
    {
        $curl = new Curl();
        $curl->setHeader('key', $this->api_key);
        $curl->post($this->base_url . '/waybill', [
            'waybill' => $resi,
            'courier' => $courier
        ]);
        if (!$curl->error) {
            return collect($curl->response->rajaongkir->result);
        }
        return collect([]);
    }
}
