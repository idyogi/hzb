<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;

class ClearImageCacheCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cache:clearimage';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Clear Image Cache; example for QR Code';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $files = File::files(public_path('qrcode'));

        $files = array_filter($files, function ($file) {
            return $file->getFilename() == '.gitignore' ? false : true;
        });

        foreach ($files as $file) {
            File::delete($file->getPathname());
        }
    }
}
