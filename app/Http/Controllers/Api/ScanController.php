<?php

namespace App\Http\Controllers\Api;

use App\History;
use App\Http\Controllers\Controller;
use App\Inventory;
use App\Sku;
use App\Hzb;
use Illuminate\Http\Request;
use SimpleSoftwareIO\QrCode\Facades\QrCode;
use Illuminate\Support\Facades\Auth;

class ScanController extends Controller
{
    public function warehouse()
    {
        // cek tipe user
        // cek apakah request sudah benar json
        // cek apakah produk terdaftar
        // cek apakah sudah ada di tabel inventory
        // buat history
        // tambah ke bagian stock sku

        if (!Auth::user()->can('scan.qrcode.warehouse')) {
            return response()->json([
                'status' => 'error',
                'message' => 'You\'re not allowed to do this'
            ], 401);
        }

        $json = request('code', null);
        $json = json_decode($json);
        if ($json == null) {
            return response()->json([
                'status' => 'error',
                'message' => 'Invalid Json Code'
            ], 401);
        }

        $product_vars = Sku::where('sku', $json->code)->first();

        if (empty($product_vars)) {
            return response()->json([
                'status' => 'error',
                'message' => 'Code not registered'
            ], 401);
        }

        $exist = Inventory::where([
            'sku' => $json->code,
            'unique' => $json->unique,
        ])->count();

        if ($exist) {
            return response()->json([
                'status' => 'error',
                'message' => 'Entry Duplicated'
            ], 401);
        }

        Inventory::create([
            'sku' => $json->code,
            'unique' => $json->unique,
            'status' => 'warehouse'
        ]);

        $product_vars->increment('qty_warehouse');
        $product_vars->save();

        $history = History::firstOrCreate([
            'sku_id' => $product_vars->id,
            'type' => 'gudang',
            'message' => '',
            'user_id' => Auth::id(),
            'date' => date('Y-m-d')
        ], [
            'count' => 0
        ]);

        $history->increment('count');
        $history->save();

        return response()->json([
            'status' => 'success',
            'message' => 'Berhasil menambahkan produk ke gudang.',
            'product_name' => optional($product_vars->product())->name,
            'quantity' => $product_vars->qty_warehouse,
        ], 200);
    }

    public function storefront()
    {
        if (!Auth::user()->can('scan.qrcode.etalase')) {
            return response()->json([
                'status' => 'error',
                'message' => 'You\'re not allowed to do this'
            ], 401);
        }

        $code = request('code', null);
        $json = json_decode($code);
        if ($json == null) {
            return response()->json([
                'status' => 'error',
                'message' => 'Invalid Json Code'
            ], 401);
        }

        $product_vars = Sku::where('sku', $json->code)->first();

        if (empty($product_vars)) {
            return response()->json([
                'status' => 'error',
                'message' => 'Code not registered'
            ], 401);
        }

        if ($product_vars->qty_warehouse == 0) {
            return response()->json([
                'status' => 'error',
                'message' => 'Barang di gudang sudah habis'
            ], 401);
        }

        $exist = Inventory::where([
            'sku' => $json->code,
            'unique' => $json->unique,
            'status' => 'warehouse'
        ]);

        if (!$exist->count()) {
            return response()->json([
                'status' => 'error',
                'message' => 'Items not on warehouse'
            ], 401);
        }

        $item = $exist->first();
        $item->status = 'storefront';
        $item->save();

        $product_vars->increment('qty_stock');
        $product_vars->decrement('qty_warehouse');
        $product_vars->save();

        $history = History::firstOrCreate([
            'sku_id' => $product_vars->id,
            'type' => 'etalase',
            'message' => '',
            'count' => 1,
            'user_id' => Auth::id(),
            'date' => date('Y-m-d')
        ], [
            'count' => 0
        ]);

        $history->increment('count');

        return response()->json([
            'status' => 'success',
            'message' => 'Berhasil menampilkan di etalase.',
            'product_name' => optional($product_vars->product())->name,
            'quantity' => $product_vars->qty_stock
        ], 200);
    }
    public function samplewarehouse()
    {
        // cek tipe user
        // cek apakah request sudah benar json
        // cek apakah produk terdaftar
        // cek apakah sudah ada di tabel inventory
        // buat history
        // tambah ke bagian stock sku


        $sku = request('sku', null);
        $unique = request('unique', null);
        $product_vars = Sku::where('sku', $sku)->first();

        if (empty($product_vars)) {
            return response()->json([
                'status' => 'error',
                'message' => 'Code not registered'
            ], 401);
        }

        $exist = Inventory::where([
            'sku' => $sku,
            'unique' => $unique
        ])->count();

        if ($exist) {
            return response()->json([
                'status' => 'error',
                'message' => 'Entry Duplicated'
            ], 401);
        }

        Inventory::create([
            'sku' => $sku,
            'unique' => $unique,
            'status' => 'warehouse'
        ]);

        $product_vars->increment('qty_warehouse');
        $product_vars->save();

        $history = History::firstOrCreate([
            'sku_id' => $product_vars->id,
            'type' => 'gudang',
            'message' => $unique,
            'user_id' => 1,
            'date' => date('Y-m-d')
        ], [
            'count' => 0
        ]);

        $history->increment('count');
        $history->save();

        return response()->json([
            'status' => 'success',
            'message' => 'Berhasil menambahkan produk ke gudang.',
            'product_name' => optional($product_vars->product())->name,
            'quantity' => $product_vars->qty_warehouse,
        ], 200);
    }

    public function samplestorefront()
    {
        $sku = request('sku', null);
        $unique = request('unique', null);

        $product_vars = Sku::where('sku', $sku)->first();

        if (empty($product_vars)) {
            return response()->json([
                'status' => 'error',
                'message' => 'Code not registered'
            ], 401);
        }

        if ($product_vars->qty_warehouse == 0) {
            return response()->json([
                'status' => 'error',
                'message' => 'Barang di gudang sudah habis'
            ], 401);
        }

        $exist = Inventory::where([
            'sku' => $sku,
            'unique' => $unique,
            'status' => 'warehouse'
        ]);

        if (!$exist->count()) {
            return response()->json([
                'status' => 'error',
                'message' => 'Items not on warehouse'
            ], 401);
        }

        $item = $exist->first();
        $item->status = 'storefront';
        $item->save();

        $product_vars->increment('qty_stock');
        $product_vars->decrement('qty_warehouse');
        $product_vars->save();

        $history = History::firstOrCreate([
            'sku_id' => $product_vars->id,
            'type' => 'etalase',
            'message' => $unique,
            'count' => 1,
            'user_id' => 1,
            'date' => date('Y-m-d')
        ], [
            'count' => 0
        ]);

        $history->increment('count');

        return response()->json([
            'status' => 'success',
            'message' => 'Berhasil menampilkan di etalase.',
            'product_name' => optional($product_vars->product())->name,
            'quantity' => $product_vars->qty_stock
        ], 200);
    }

    public function sample()
    {
        $code = Sku::inRandomOrder()->first();
        $json = json_encode(['code' => $code->sku, 'unique' => uniqid()]);
        return QrCode::size(300)->generate($json);
    }

    public function hzb()
    {
        $kode = request('kode', null);
        $event = request('event', null);
        if (empty($kode)) {
            return ['status' => 'error', 'message' => 'Kode dibutuhkan.'];
        }
        if (empty($event)) {
            return ['status' => 'error', 'message' => 'Event dibutuhkan.'];
        }
        $hzb = Hzb::Create([
            'kode' => $kode,
            'event' => $event
        ]);
        return [
            'status' => 'success',
            'message' => 'Berhasil.'
        ];
    }
}
