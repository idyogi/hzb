<?php

namespace App\Http\Middleware;

use App\Device;
use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\App;

class ApiToken
{
    /**
     * Handle an incoming request
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $token = request()->header('X-Authorization');

        if ($token) {
            $device = Device::where('token', $token)->first();
            if ($device) {
                Auth::loginUsingId($device->user_id);
            } else {
                return response()->json(['status' => 'error', 'message' => 'Invalid Token'], 401);
            }
        } elseif ($request->has('force_user') && App::environment('local')) {
            Auth::loginUsingId($request->force_user);
        } else {
            return response()->json(['status' => 'error', 'message' => 'Token not provided'], 401);
        }

        $response = $next($request);

        Auth::logout();

        return $response;
    }
}
