<?php

namespace App\Http\Resources;

use App\OrderItem;
use Illuminate\Http\Resources\Json\JsonResource;

class CheckoutResource extends JsonResource
{
    public static $wrap = 'items';
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $items = OrderItem::where('order_id', $this->id)->get();
        return $items;
    }

    public function with($request)
    {
        return [
            'user' => 'y',
            'address' => ''
        ];
    }
}
