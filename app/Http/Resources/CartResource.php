<?php

namespace App\Http\Resources;

use App\Sku;
use App\Product;
use Illuminate\Http\Resources\Json\JsonResource;

class CartResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $product = Product::find($this->product_id);
        $sku = Sku::find($this->sku_id);
        $model = $sku->values()->get()->map(function ($value) {
            return $value->name;
        });
        $price = ($sku->price * $this->qty) + ($this->cut * $this->qty * 15000);

        return [
            'id_cart' => $this->id,
            'notes' => $this->notes,
            'cut' => $this->cut,
            'cut_notes' => $this->cut_notes,
            'product_name' => $product->name,
            'product_image' => $product->image()->url,
            'product_model' => implode(',', $model->toArray()),
            'qty' => $this->qty,
            'price' => $price,
            'price_text' => 'Rp. ' . number_format($price, 0, ',', '.')
        ];
    }
}
