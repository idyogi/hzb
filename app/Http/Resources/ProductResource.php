<?php

namespace App\Http\Resources;

use App\Brand;
use App\Attachment;
use Illuminate\Http\Resources\Json\JsonResource;

class ProductResource extends JsonResource
{

    public function __construct($resource)
    {
        parent::__construct($resource);
    }
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'slug' => $this->slug,
            'description' => $this->description,
            'images' => Attachment::where('product_id', $this->id)->pluck('url'),
            'categories' => [],
            'price' => $this->getLowestPrice(),
            'price_text' => 'Rp. ' . number_format($this->getLowestPrice(), 0, ',', '.'),
            'variations' => $this->getVariants(),
            // 'models' => $this->getModels(),
            'matching' => $this->getMatching(),
            'price_sale' => $this->price_sale,
            'preorder' => $this->preorder,
            'preorder_date' => $this->preorder_date,
            'preorder_days' => $this->preorder_days,
            'qty_stock' => $this->qty_stock,
            'qty_warehouse' => $this->qty_warehouse,
            'sku' => $this->sku,
            'shipping' => [
                'weight' => $this->weight,
                'length' => $this->length,
                'width' => $this->width,
                'height' => $this->height,
            ],
            'purcashe_note' => $this->purcashe_note,
            'location' => $this->location,
            'cut_option' => $this->cut_option,
            'conditions' => $this->conditions,
            'brand_name' => optional(Brand::find($this->brand_id))->name,
            'app_only' => $this->app_only,
            'parent_id' => $this->parent_id,
            'created_at' => $this->created_at->format('Y-m-d H:i:s'),
            'links' => [
                'product' => route('api.product', $this->id),
                'add_cart' => route('api.cart.store', ['product' => $this->id, 'qty' => 1])
            ]
        ];
    }

    public function getVariants()
    {
        $variations = $this->variations()->get();

        $results = [];
        foreach ($variations as $variant) {
            $results[$variant->name] = $variant->values()->pluck('name', 'id')->toArray();
        }

        return $results;
    }
}
