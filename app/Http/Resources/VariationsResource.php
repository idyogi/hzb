<?php

namespace App\Http\Resources;

use App\Attachment;
use App\Brand;
use App\User;
use Illuminate\Http\Resources\Json\JsonResource;

class VariationsResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'price' => $this->price,
            'price_text' => 'Rp. ' . number_format($this->price,0,',','.'),
            'variations' => $this->getVariants(),
            'models' => $this->getModels(),
            'price_sale' => $this->price_sale,
            'stock' => $this->stock,
            'sku' => $this->sku
        ];
    }
}
