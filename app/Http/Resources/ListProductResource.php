<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ListProductResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $price = $this->getLowestPrice();
        $price_sale = $this->getLowestPriceSale();

        return [
            'id' => $this->id,
            'name' => $this->name,
            'description' => $this->description,
            'images' => $this->images()->map(function ($image) {
                return $image->url;
            }),
            'price' => $price,
            'price_text' => 'Rp.' . number_format($price, 0, ',', '.'),
            'price_sale' => $price_sale,
            'price_sale_text' => 'Rp.' . number_format($price_sale, 0, ',', '.'),
            'qty_stock' => $this->getQtyStock(),
            'product_url' => route('api.product', $this->id)
        ];
    }
}
