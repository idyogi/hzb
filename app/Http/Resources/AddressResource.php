<?php

namespace App\Http\Resources;

use Illuminate\Support\Str;
use Illuminate\Http\Resources\Json\JsonResource;

class AddressResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name . ' | ' . $this->phone,
            'address' => $this->address . "\n" . Str::upper(sprintf("%s - %s, %s, %s", $this->city, $this->subdistrict, $this->province, $this->postal_code))
        ];
    }
}
