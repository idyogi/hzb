<?php

namespace App\Providers;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);

        if (env('APP_DEBUG', true)) {
            DB::listen(function ($query) {
                File::append(
                    storage_path('/logs/query.log'),
                    sprintf("[%s][%s] %s [%s]\n", date('H:i:s'), $query->time, $query->sql, implode(', ', $query->bindings))
                );
            });
        }
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
