<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Category;

class CategoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categories = ['Beauty','Men Wear','Clothing','Scraf','Accessories','Bag','Shoes','Praying Set','Socks','Gift Set'];

        foreach($categories as $cat) {
            Category::create([
                'name' => $cat,
                'slug' => str_slug($cat),
                'parent_id' => '0'
            ]);
        }

    }
}
