<?php

namespace Tests\Browser;

use App\User;
use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\WithFaker;

class CreateProductTest extends DuskTestCase
{
    use WithFaker;
    /**
     * @test
     */
    public function createProduct()
    {
        $this->browse(function (Browser $browser) {
            $user = User::where('status', 'admin')->first();
            $browser->loginAs($user)
                ->visitRoute('admin.product.create')
                ->type('name', 'Produk ' . uniqid())
                ->type('description', $this->faker->sentence)
                ->type('publish_date', '2019-04-03')
                ->type('publish_date_vip', '2019-04-02')
                ->radio('price_type', 'normal')
                ->type('weight', '100')
                ->type('length', '100')
                ->type('width', '100')
                ->type('height', '100')
                ->check('cut_option', true)
                ->check('app_only', true)
                ->check('preorder', true)
                ->press('Save')
                ->assertSee('Deskripsi');
        });
    }
}
