<?php

namespace Tests\Feature;

use App\User;
use App\Product;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ApiOrderTest extends TestCase
{
    use WithFaker;
    use DatabaseTransactions;

    private function getUserToken($user_id)
    {
        $user = User::find($user_id);

        $response = $this->json('POST', '/api/login', [
            'email' => $user->email,
            'password' => 'secret'
        ]);

        $response = json_decode($response->getContent());

        return $response->token;
    }

    /**
     * @test
     */
    public function userOrderItems()
    {
        $product = Product::find(1);

        $response = $this->withHeaders([
            'X-Authorization' => $this->getUserToken(4)
        ])->json('POST', 'api/cart/store', [
            'product_id' => $product->id,
            'sku_id' => $product->skus->first()->id,
            'qty' => 1,
            'notes' => 'tidak di potong',
            'cut' => 0,
            'cut_notes' => '',
        ]);

        $response->assertJson(['status' => 'success'])->assertStatus(200);
    }

    /**
     * @test
     */
    public function userOrderItemsWithInvalidParam()
    {
        $product = Product::find(1);

        $response = $this->withHeaders([
            'X-Authorization' => $this->getUserToken(4)
        ])->json('POST', 'api/cart/store', [
            'product_id' => $product->id,
            'sku_id' => $product->skus->first()->id,
            'qty' => 'satu',
        ]);

        $response->assertJson(['status' => 'error'])->assertStatus(401);
    }

    /**
     * @test
     */
    public function errorWhenUserOrderMoreThanStock()
    {
        $product = Product::find(1);

        $response = $this->withHeaders([
            'X-Authorization' => $this->getUserToken(4)
        ])->json('POST', 'api/cart/store', [
            'product_id' => $product->id,
            'sku_id' => $product->skus->first()->id,
            'qty' => 9999,
            'notes' => 'yang bagus ya',
            'cut' => 1,
            'cut_notes' => 'potong 5cm',
        ]);

        $response
            ->assertJson(['status' => 'error'])
            ->assertJsonStructure(['status', 'message', 'qty_stock'])
            ->assertStatus(401);
    }

    /**
     * @test
     */
    public function errorWhenUserOrderNotOnRightVariant()
    {
        $product = Product::find(1);

        $response = $this->withHeaders([
            'X-Authorization' => $this->getUserToken(4)
        ])->json('POST', 'api/cart/store', [
            'product_id' => $product->id,
            'sku_id' => 999,
            'qty' => 6,
            'notes' => 'yang bagus ya',
            'cut' => 1,
            'cut_notes' => 'potong 5cm',
        ]);

        $response->assertJson(['status' => 'error'])->assertStatus(401);
    }

    /**
     * @test
     */
    public function userChangeOrder()
    {
        $product = Product::find(1);

        $response = $this->withHeaders([
            'X-Authorization' => $this->getUserToken(4)
        ])->json('POST', 'api/cart/store', [
            'product_id' => $product->id,
            'sku_id' => $product->skus->first()->id,
            'qty' => 1,
            'notes' => $this->faker->sentence(),
            'cut' => 1,
            'cut_notes' => 'potong 5cm',
        ]);

        $response->assertJson(['status' => 'success'])->assertStatus(200);

        $response = $this->withHeaders([
            'X-Authorization' => $this->getUserToken(4)
        ])->json('POST', 'api/cart/update', [
            'product_id' => $product->id,
            'sku_id' => $product->skus->first()->id,
            'qty' => 4,
            'notes' => $this->faker->sentence(),
            'cut' => 0,
            'cut_notes' => 'gak potong',
        ]);

        $response->assertJson(['status' => 'success'])->assertStatus(200);
    }

    /**
     * @test
     */
    public function userClearCart()
    {
        $response = $this->withHeaders([
            'X-Authorization' => $this->getUserToken(4)
        ])->json('GET', 'api/cart/clear');

        $response
            ->assertJson(['status' => 'success'])
            ->assertStatus(200);
    }

    /**
     * @test
     */
    public function userGetCart()
    {
        $product = Product::InRandomOrder()->first();

        $response = $this->withHeaders([
            'X-Authorization' => $this->getUserToken(4)
        ])->json('POST', 'api/cart/store', [
            'product_id' => $product->id,
            'sku_id' => $product->skus->first()->id,
            'qty' => 5,
            'notes' => 'pesan ganteng',
            'cut' => 1,
            'cut_notes' => 'potong 100cm',
        ]);

        $response
            ->assertJson(['status' => 'success'])
            ->assertStatus(200);

        $response = $this->withHeaders([
            'X-Authorization' => $this->getUserToken(4)
        ])->json('GET', 'api/cart/get');

        $response
            ->assertJson(['status' => 'success'])
            ->assertSee('pesan ganteng')
            ->assertSee(5)
            ->assertSee('potong 100cm')
            ->assertJsonStructure(['status', 'data', 'message'])
            ->assertStatus(200);
    }


    public function testUserCheckout()
    {
        $product = Product::InRandomOrder()->first();
        $user_token = $this->getUserToken(4);

        $response = $this->withHeaders([
            'X-Authorization' => $this->getUserToken(4)
        ])->json('POST', 'api/cart/store', [
            'product_id' => $product->id,
            'sku_id' => $product->skus->first()->id,
            'qty' => 2,
            'notes' => 'pesan ganteng',
            'cut' => 1,
            'cut_notes' => 'potong 100cm',
        ]);

        $response
            ->assertJson(['status' => 'success'])
            ->assertStatus(200);

        $response = $this->withHeaders([
            'X-Authorization' => $this->getUserToken(4)
        ])->json('GET', 'api/checkout/get');

        $response
            ->assertJsonStructure([
                'address',
                'items',
                'shipping_service',
                'shipping_etd',
                'payment_type',
                'payment_image_url',
                'subtotal_items',
                'subtotal_shipping',
                'total',
                'address_ok',
                'shipping_ok',
                'payment_ok',
                'create_order'
            ])
            ->assertStatus(200);
    }

    public function testUserCheckoutThenChangeAddressAndPayment()
    {
        $product = Product::InRandomOrder()->first();
        $user_token = $this->getUserToken(4);

        $response = $this->withHeaders([
            'X-Authorization' => $user_token
        ])->json('POST', 'api/cart/store', [
            'product_id' => $product->id,
            'sku_id' => $product->skus->first()->id,
            'qty' => 2,
            'notes' => 'pesan ganteng',
            'cut' => 1,
            'cut_notes' => 'potong 100cm',
        ]);

        $response
            ->assertJson(['status' => 'success'])
            ->assertStatus(200);

        $response = $this->withHeaders([
            'X-Authorization' => $user_token
        ])->json('GET', 'api/checkout/get');

        $response->assertJson([
            'payment_type' => null,
            'create_order' => false,
            'address_ok' => true,
            'shipping_ok' => true,
            'payment_ok' => false
        ]);

        $response = $this->withHeaders([
            'X-Authorization' => $user_token
        ])->json('POST', 'api/checkout/update/address', [
            'id' => 2
        ]);

        $response
            ->assertJson(['status' => 'success', 'message' => 'Address updated'])
            ->assertStatus(200);

        $response = $this->withHeaders([
            'X-Authorization' => $user_token
        ])->json('POST', '/api/checkout/update/payment', [
            'id' => 'bank_transver_permata'
        ]);

        $response
            ->assertJson([
                'status' => 'success',
                'message' => 'Payment updated'
            ])
            ->assertStatus(200);

        $response = $this->withHeaders([
            'X-Authorization' => $user_token
        ])->json('GET', 'api/checkout/get');

        $response
            ->assertJson([
                'payment_type' => 'bank_transver_permata',
                'create_order' => true,
                'address_ok' => true,
                'shipping_ok' => true,
                'payment_ok' => true
            ])
            ->assertStatus(200);
    }
}
