<?php

namespace Tests\Feature;

use App\Favorite;
use App\Product;
use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ApiFavotireTest extends TestCase
{
    private function getUserToken($user_id)
    {
        $user = User::find($user_id);

        $response = $this->json('POST', '/api/login', [
            'email' => $user->email,
            'password' => 'secret'
        ]);

        $response = json_decode($response->getContent());

        return $response->token;
    }

    /**
     * @test
     */
    public function clearFavorites()
    {
        Favorite::where('user_id', 1)->delete();

        $response = Favorite::where('user_id', 1);

        $this->assertSame(0, $response->count());
    }

    /**
     * @test
     */
    public function addItemToFavorite()
    {
        $item = Product::first();

        $response = $this->withHeaders([
            'X-Authorization' => $this->getUserToken(1)
        ])->json('POST', '/api/favorite/store', [
            'id' => $item->id
        ]);

        $response
            ->assertJson(['status' => 'success'])
            ->assertStatus(201);
    }

    /**
     * @test
     */
    public function cannotAddFavoriteTwice()
    {
        $item = Product::first();

        $response = $this->withHeaders([
            'X-Authorization' => $this->getUserToken(1)
        ])->json('POST', '/api/favorite/store', [
            'id' => $item->id
        ]);

        $response
            ->assertJson(['status' => 'error', 'message' => 'Item has already on Favorite'])
            ->assertStatus(401);
    }

    /**
     * @test
     */
    public function getFavorites()
    {
        $item = Product::first();

        $response = $this->withHeaders([
            'X-Authorization' => $this->getUserToken(1)
        ])->json('GET', '/api/favorite/get');

        $response
            ->assertSee($item->name)
            ->assertStatus(200);
    }

    /**
     * @test
     */
    public function cannotDestroyNotExistingFavorite()
    {
        $response = $this->withHeaders([
            'X-Authorization' => $this->getUserToken(1)
        ])->json('POST', '/api/favorite/destroy/9999999999');

        $response
            ->assertJson(['status' => 'error'])
            ->assertStatus(401);
    }

    /**
     * @test
     */
    public function destroyFavorites()
    {
        $item = Product::first();

        $response = $this->withHeaders([
            'X-Authorization' => $this->getUserToken(1)
        ])->json('POST', '/api/favorite/destroy/' . $item->id);

        $response
            ->assertJson(['status' => 'success'])
            ->assertStatus(200);
    }
}
