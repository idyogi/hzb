<?php

namespace Tests\Feature;

use App\User;
use App\Address;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ApiAddressTest extends TestCase
{
    use WithFaker;

    private function getUserToken($user_id)
    {
        $user = User::find($user_id);

        $response = $this->json('POST', '/api/login', [
            'email' => $user->email,
            'password' => 'secret'
        ]);

        $response = json_decode($response->getContent());

        return $response->token;
    }

    public function testGetProvince()
    {
        $response = $this->json('GET', '/api/address/province');

        $response
            ->assertJsonFragment(['province_id' => '2', 'province' => 'Bangka Belitung'])
            ->assertStatus(200);
    }

    public function testGetCity()
    {
        $response = $this->json('GET', '/api/address/city?id=5'); // get city of yogyakarta

        $response
            ->assertJsonFragment([
                'city_id' => '419',
                'province_id' => '5',
                'province' => 'DI Yogyakarta',
                'type' => 'Kabupaten',
                'city_name' => 'Sleman',
                'postal_code' => '55513'
            ])
            ->assertStatus(200);
    }

    public function testGetSubdistrict()
    {
        $response = $this->json('GET', '/api/address/subdistrict?id=419'); // get subdistrict of sleman

        $response
            ->assertJsonFragment([
                'subdistrict_id' => '5780',
                'province_id' => '5',
                'province' => 'DI Yogyakarta',
                'city_id' => '419',
                'city' => 'Sleman',
                'type' => 'Kabupaten',
                'subdistrict_name' => 'Cangkringan'
            ])
            ->assertStatus(200);
    }

    public function testCreateAddress()
    {
        $response = $this->withHeaders([
            'X-Authorization' => $this->getUserToken(4)
        ])->json('POST', '/api/address/store', [
            'alias' => 'Rumah Kontrakan',
            'name' => $this->faker->firstName(),
            'province_id' => '5',
            'city_id' => '501',
            'subdistrict_id' => '6983',
            'address' =>  $this->faker->address,
            'postal_code' => rand(10000, 99999),
            'phone' => '082221114471',
            'is_default' => '1'
        ]);

        $response
            ->assertJson(['status' => 'success'])
            ->assertStatus(201);
    }

    public function testUpdateAddress()
    {
        $address = Address::first();

        $response = $this->withHeaders([
            'X-Authorization' => $this->getUserToken(4)
        ])->json('POST', '/api/address/update', [
            'id' => $address->id,
            'alias' => 'Rumah Robbyn',
            'name' => $this->faker->firstName,
            'province_id' => '5',
            'city_id' => '501',
            'subdistrict_id' => '6983',
            'address' => $this->faker->address,
            'postal_code' => rand(10000, 99999),
            'phone' => $this->faker->phoneNumber,
            'is_default' => '1'
        ]);

        $response
            ->assertJson(['status' => 'success'])
            ->assertStatus(200);
    }

    public function testInvalidCreateParams()
    {
        $response = $this->withHeaders([
            'X-Authorization' => $this->getUserToken(4)
        ])->json('POST', '/api/address/store', [
            'name' => $this->faker->name,
            'province_id' => '5',
            'city_id' => '501',
            'subdistrict_id' => '6983',
            'address' => $this->faker->address,
            'postal_code' => rand(10000, 99999),
            'phone' => $this->faker->phoneNumber,
            'is_default' => '1'
        ]);

        $response
            ->assertJson(['status' => 'error'])
            ->assertJsonStructure(['status', 'message', 'validator'])
            ->assertStatus(401);
    }

    public function testUpdateInvalidAddress()
    {
        $response = $this->withHeaders([
            'X-Authorization' => $this->getUserToken(4)
        ])->json('POST', '/api/address/update', [
            'id' => '999999',
            'alias' => 'Rumah Robbyn',
            'name' => $this->faker->name,
            'province_id' => '5',
            'city_id' => '501',
            'subdistrict_id' => '6983',
            'address' => $this->faker->address,
            'postal_code' => rand(10000, 99999),
            'phone' => $this->faker->phoneNumber,
            'is_default' => '1'
        ]);

        $response
            ->assertJson(['status' => 'error', 'message' => 'Nothing to update'])
            ->assertStatus(401);
    }

    public function testGetListAddress()
    {
        $response = $this->withHeaders([
            'X-Authorization' => $this->getUserToken(4)
        ])->json('GET', '/api/address/get');

        $response
            ->assertJson(['status' => 'success'])
            ->assertStatus(200);
    }

    public function testDeleteAddress()
    {
        $address = Address::first();

        $response = $this->withHeaders([
            'X-Authorization' => $this->getUserToken(4)
        ])->json('POST', '/api/address/destroy', [
            'id' => $address->id
        ]);

        $response
            ->assertJson(['status' => 'success'])
            ->assertStatus(200);
    }
}
