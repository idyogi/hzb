<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ApiWithoutLoginTest extends TestCase
{
    /**
     * @test
     */
    public function listProduct()
    {
        $response = $this->json('GET', '/api/products');

        $response->assertStatus(200);
    }

    /**
     * @test
     */
    public function detailProduct()
    {
        $response = $this->json('GET', '/api/product/1');

        $response->assertStatus(200);
    }
}
