<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\External\Payment\Bca;

class BcaPaymentTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testLoginWithBca()
    {
        $bca = new Bca();
        $response = $bca->login();

        $this->assertSame(54, strlen($response->body->access_token));
        $this->assertTrue($response->body ? true : false);
    }

    public function testGetBalanceInformation()
    {
        $bca = new Bca();
        // $response = $bca->login();
        $sample_account = ['0201245680'];

        $response = $bca->getBalance($sample_account);

        // dump($response);
        $this->assertTrue(true);
    }
}
